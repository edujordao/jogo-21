package com.company;

import java.util.HashMap;
import java.util.Random;

public class Baralho {

    public static HashMap<Numeros, Naipes> iniciarBaralho(){

        HashMap<Numeros, Naipes> baralho = new HashMap<>();

        for(int i=0; i< Naipes.values().length; i++){

            for(int j=0; j< Numeros.values().length; j++) {
                baralho.put(Numeros.values()[j],Naipes.values()[i]);
            }
        }

        return baralho;
    }
}
