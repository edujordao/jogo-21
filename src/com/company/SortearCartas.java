package com.company;

import java.util.Random;

public class SortearCartas {

    Random sorteio = new Random();

    public Naipes sortearNaipe()
    {
        Naipes naipeSorteado = Naipes.values()[sorteio.nextInt(Naipes.values().length)];
        return naipeSorteado;
    }

    public Numeros sortearNumero()
    {
        Numeros numeroSorteado = Numeros.values()[sorteio.nextInt(Numeros.values().length)];
        return numeroSorteado;
    }
}