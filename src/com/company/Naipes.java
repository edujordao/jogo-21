package com.company;

public enum Naipes {

    OURO(1),
    ESPADA(2),
    COPAS(3),
    PAUS(4);

    private int value;

    Naipes(int i) {
        this.value = i;
    }

    public int getValue() {
        return value;
    }
}
