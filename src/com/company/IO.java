package com.company;

import java.util.Scanner;

public class IO {

    public String solicitarInformacao(String frase) {

        String informacao;

        Scanner scanner = new Scanner(System.in);
        System.out.println(frase);
        informacao = scanner.next();

        return informacao;
    }

    public void exibirInformacao(String mensagem){
        System.out.println(mensagem);
    }
}